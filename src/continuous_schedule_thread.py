import threading
import time


class ScheduleThread(threading.Thread):
    def __init__(self, local_schedule, break_interval=1):
        super().__init__()
        self.local_schedule = local_schedule
        self.break_interval = break_interval
        self.should_exit = False

    def run(self):
        while not self.should_exit:
            self.local_schedule.run_pending()
            time.sleep(self.break_interval)