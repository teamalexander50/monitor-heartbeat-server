from sys import argv, exit
from heartbeat_server import HeartbeatServer
from threaded_http_server import ThreadedHTTPServer
from continuous_schedule_thread import ScheduleThread
import logger
import schedule

PORT = 6474


def run(server_class=ThreadedHTTPServer, handler_class=HeartbeatServer, port=PORT):
    logger.log_message("---BOOTING---", False, True)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    schedule_thread = ScheduleThread(schedule)
    schedule_thread.start()
    logger.log_message("Heartbeat Server Listening", True, True)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        schedule_thread.should_exit = True
        exit("User Terminated Program")


if __name__ == "__main__":
    if len(argv) == 2:
        run(port=int(argv[1]))
        pass
    else:
        run()
