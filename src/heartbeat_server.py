from http.server import BaseHTTPRequestHandler
import threading
import logger
import schedule
import requests

HEARTBEAT_POST_URL = "http://172.20.251.74/Sensor/Heartbeat"
LISTENER_DELAY_MINUTES = 58


class HeartbeatServer(BaseHTTPRequestHandler):
    def _set_headers(self, status_code=200):
        self.send_response(status_code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        connection_msg = "Connection from " + str(self.client_address[0]) + " on port " + str(self.client_address[1])
        logger.log_message(connection_msg, True, True)
        try:
            data = self.parse_parameters(self.rfile.read(int(self.headers.get("Content-Length"))))
            data['online'] = 'true'
            or_id, sensor_id = self.parse_identifiers(data)
            if or_id == -1 or sensor_id == -1:
                message = "Operating Room ID and Sensor ID Required"
                self._set_headers(status_code=406)
                self.wfile.write(str.encode(message))
            else:
                requests.patch(HEARTBEAT_POST_URL, params=data)
                self.schedule_listen(or_id, sensor_id)
                message = "Heartbeat for Operating Room ID = " + str(or_id) + ", Sensor ID = " + str(sensor_id)
                self._set_headers()
                self.wfile.write(str.encode(message))
        except ValueError as error:
            message = "Error: " + str(error)
            self._set_headers(status_code=406)
            self.wfile.write(str.encode(message))
        except Exception as error:
            message = "Error: " + str(error)
            self._set_headers(status_code=500)
            self.wfile.write(str.encode(message))

        logger.log_message(message, True, True)

    @staticmethod
    def parse_parameters(encoded_parameters):
        data = {}
        try:
            decoded_parameters = encoded_parameters.decode("utf-8")
            parameters = decoded_parameters.split("&")
            for parameter in parameters:
                key, value = parameter.split("=")
                value = value.replace("+", " ")
                data[key.lower()] = value.lower()
        except ValueError:
            raise ValueError("Operating Room ID and Sensor ID are required")
        return data

    @staticmethod
    def parse_identifiers(decoded_parameters):
        if 'or_id' in decoded_parameters and 'sensor_id' in decoded_parameters:
            or_id = decoded_parameters['or_id']
            sensor_id = decoded_parameters['sensor_id']
            try:
                if int(or_id) < 0 or int(sensor_id) < 0:
                    raise ValueError()
            except ValueError:
                msg = "Operating Room ID (" + or_id + ") and Sensor ID (" + sensor_id + ") must be nonnegative integers"
                raise ValueError(msg)
            else:
                return or_id, sensor_id
        else:
            raise ValueError('Operating Room ID and Sensor ID are required')

    def schedule_listen(self, or_id, sensor_id):
        schedule.every(LISTENER_DELAY_MINUTES).minutes.do(self.threaded_job, self.repeat_listener, or_id, sensor_id)

    @staticmethod
    def threaded_job(job, or_id, sensor_id):
        job_thread = threading.Thread(target=job, args=(or_id, sensor_id))
        job_thread.start()
        return schedule.CancelJob

    @staticmethod
    def repeat_listener(or_id, sensor_id):
        parameters = {'or_id': or_id, 'sensor_id': sensor_id, 'online': 'false'}
        requests.patch(HEARTBEAT_POST_URL, params=parameters)
        message = "Operating Room ID = " + str(or_id) + ", Sensor ID = " + str(sensor_id) + " is Offline"
        logger.log_message(message, True, True)
