from datetime import datetime

LOG_FILE_PATH = "debug.log"


def log_message(message, to_console, to_file):
    message = datetime.now().strftime("[%H:%M:%S, %m/%d/%Y]") + " " + message
    if not message.endswith("\n"):
        message += "\n"

    if message == "":
        return

    if to_console:
        print(message)

    if to_file:
        log_file = open(LOG_FILE_PATH, 'a')
        log_file.write(message)
        log_file.close()
